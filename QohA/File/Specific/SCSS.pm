package QohA::File::Specific::SCSS;

use Modern::Perl;
use Moo;
use IPC::Cmd qw[run];
use File::Basename;
use File::Temp qw[tempfile];
extends 'QohA::File';

sub run_checks {
    my ($self, $cnt) = @_;
    my @r = $self->check_css_scss_sync();
    $self->SUPER::add_to_report('css_and_scss_in_sync', \@r);

    return $self->SUPER::run_checks($cnt);
}

sub check_css_scss_sync {
    my ($self) = @_;
    return 0 unless -e $self->path;

    # Don't check on first pass
    return 1 if $self->pass == 0;

    open ( my $fh, $self->path )
        or die "I cannot open $self->path ($!)";

    my $dir = dirname( $self->abspath );
    my $cmd =
          q{yarn build }
        . ( $self->path =~ /opac-tmpl/ ? '--view=opac' : '' )
    ;
    my ( $success, $error_code, $full_buf, $stdout_buf, $stderr_buf ) =
        run( command => $cmd, verbose => 0 );

    $cmd = q{git status -suno|grep -P '/css/.*\.css'|cut -d" " -f3};
    ( $success, $error_code, $full_buf, $stdout_buf, $stderr_buf ) =
        run( command => $cmd, verbose => 0 );

    return 1 unless scalar @$full_buf;

    # Remove the changes
    for my $scss_file ( @$full_buf ) {
        chomp $scss_file;
        run( command => qq{git checkout $scss_file}, verbose => 0 );
    }

    return ( "Found CSS file not in sync with its SCSS file, use yarn build. This is not blocker, testers can generate the .css file if they need it, see https://wiki.koha-community.org/wiki/Working_with_SCSS_in_the_OPAC_and_staff_client");
}

1;

=head1 AUTHOR
Jonathan Druart <jonathan.druart@bugs.koha-community.org>

=head1 COPYRIGHT AND LICENSE

This is free software, licensed under:

  The GNU General Public License, Version 3, June 2007
=cut

package QohA::File::JS;

use Modern::Perl;
use Moo;

extends 'QohA::File';

use File::Slurp qw[read_file];

sub run_checks {
    my ($self, $cnt) = @_;

    my $r = $self->check_ES_template_literals();
    $self->SUPER::add_to_report('ES template literals', $r);

    return $self->SUPER::run_checks($cnt);
}

sub check_ES_template_literals {
    my ($self) =@_;

    return q{} unless -e $self->path;
    return q{} if $self->path !~ m|/prog/js| and $self->path !~ m|/bootstrap/js|;

    my $absf = File::Spec->rel2abs($self->path);
    my @lines = read_file($absf);
    my @errors;
    for my $l ( @lines ) {
        chomp $l;
        if ( $l =~ m|`.*\$| ) {
            push @errors, sprintf 'ES template literals found (%s) - See bug 24625', $l;
        }
    }
    return \@errors;
}

1;

=head1 AUTHOR

Jonathan Druart <jonathan.druart@bugs.koha-community.org>

=head1 COPYRIGHT AND LICENSE

This is free software, licensed under:

  The GNU General Public License, Version 3, June 2007

=cut
